// oleh Jessy Faujiyyah Khairani (1207050137)
//soal bab 4 nomor 1

//1.Buatlah sebuah program sapaan dala notasi algoritma dengan spesifikasi sebagai berikut :
//-	Menampilkan tulisan "Halo, siapa namamu?" di layar, lalu
//-	Meminta pengguna memasukkan namanya, lalu
//-	Menampikan tulisan "Di kota apa kamu sekarang?"di layar, lalu
//-	Meminta pengguna memasukkan nama kotanya sekarang, dan akhirnya 
//-	Menuliskan pesan, "Senang berteman denganmu," <nama>,"di kota" <kota>, yang dalam hal ini <nama> dan <kota> adalah string yang dibaca berdasarkan b) dan d)


#include <iostream>

using namespace std;

int main (){
string nama, kota;
cout<<"Halo, siapa namamu?"<<endl;
getline(cin,nama);
cout<<"Di kota apa kamu sekarang?"<<endl;
getline(cin,kota);
cout<<"Senang berteman denganmu ,"<<nama<<",dikota "<<kota<<endl;

return 0;

}

