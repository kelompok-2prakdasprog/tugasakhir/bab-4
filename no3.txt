Nama : Dian Hasna Ramadhani
NIM : 1207050026
Kelas : Teknik Informstika / B

BAB 4
3. Buatlah program sederhana dalam notasi algoritma yang membaca temperatur dalam satuan Celcius lalu mengkonversinya ke satuan Fahrenheit, dan menampilkan hasil konversinya ke layar. Rumus dari Celsius ( C ) ke Fahrenheit ( F ) adalah F = 9/5 x C + 32.
Jawab : 
read (c) 		{inputkan nilai derajat dalam celcius}
 F 		9/5 x C + 32	{hitung derajat fahrenheit}
Write (F)		{Tampilkan nilai derajat dalam Fahrenheit}
